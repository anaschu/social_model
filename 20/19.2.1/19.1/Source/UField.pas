unit UField;

interface

uses  Ucell;

type
  TField = class // ��� ����� ���������� ��������� � ����������1
  private
    FWidth: Integer; // ������ ���� � �������
    FHeight: Integer;
    FCell: Tcell;

    //FField: TField;
  public
    // �����������.
    FArea: array of array of Tcell; // ������ ������ ����
    constructor Create(Width, Height: Integer);
    // ����������.
    procedure fillField(rowCount, colCount, maxValue: Integer);
    function IsInside(X, Y: Integer): boolean;
    function GetHeight: Integer;
    function getWidth: Integer;
    function GetCellExist(x,y:integer ):boolean;
    procedure SetCellExist(X, Y: Integer);
    property Cell: TCell read FCell;
    function getFamilyId(X, Y: integer): integer;
    procedure  SetFamilyId(X, Y: integer);
    procedure SeveralManPostcreate(x,y,i:integer);
    procedure OneManPostcreate(x,y,k,i,husband:integer);
    procedure killHuman (x,y,i:integer);
    procedure cutHuman (x,y,x1,y1,i:integer);
    procedure getID(x,y:integer);

  end;
var
FamilyCount : integer;  // ���������� �����  �� ����
manCount:integer;

implementation

procedure TField.killHuman (x,y,i:integer);
  Var
 j,ArrSize: Integer; // ���������� ��������� �� ���������� ��������� � �������.
begin

   ArrSize := length(FArea[x, y].Ffamily.APeople)-1;
   for j := i to ArrSize do FArea[x, y].Ffamily.APeople[j] := FArea[x, y].Ffamily.APeople[j+1];  //������� j-� ������� �� �������
   SetLength(FArea[x, y].Ffamily.APeople , ArrSize);

end;


procedure TField.cutHuman (x,y,x1,y1,i:integer);
  Var
 j,ArrSize,j1,ArrSize1: Integer; // ���������� ��������� �� ���������� ��������� � �������.
 Hum:THuman;
 begin
   j:=0;
   ArrSize1:=0;
   ArrSize:=0;
   ArrSize := length(FArea[x, y].Ffamily.APeople);
   ArrSize1 := length(FArea[x1, y1].Ffamily.APeople);
   if (ArrSize>0) then
   begin
     setlength(FArea[x1, y1].Ffamily.APeople,ArrSize1+1);
     FArea[x1, y1].Ffamily.APeople[ArrSize1]:=FArea[x, y].Ffamily.APeople[i];
     killHuman (x,y,i);
   end
   else
   begin
     Hum := THuman.Create;
     setlength(FArea[x1, y1].Ffamily.APeople,ArrSize1+1);
     FArea[x1, y1].Ffamily.APeople[ArrSize1]:=Hum;
     FArea[x1, y1].Ffamily.APeople[ArrSize1]:=FArea[x, y].Ffamily.APeople[i];
     killHuman (x,y,i);
   end;

end;

procedure Tfield.getID(x,y:integer);
begin
  //FArea[x, y].ParentID:= FArea[x, y].FamilyId; //��������� ����� ������( �����). ������ ������� ������ ����
  inc(FamilyCount)     ;  // ���������� �����  �� ����
  FArea[x, y].alreadyID:=true;
  FArea[x, y].FamilyId:= FamilyCount;

end;

procedure TField.OneManPostcreate(x,y,k,i,husband:integer);
{ i - ����� ������� ������� Apeople[x,y]
 husband- ����� �������
 K-������ ����� �������}
var AgrParents,delta,olreadyPeople:integer;
 Hum:THuman;
begin
SetLength(FArea[x, y].Ffamily.APeople ,K+1);
Hum := THuman.Create;
Hum.FamilyId:= Farea[x,y].familyID;
delta:=random(2);
AgrParents:= round((FArea[x, y].Ffamily.APeople[i].Agression+FArea[x, y].Ffamily.APeople[HUSBAND].Agression+0.1)/2);
Hum.Agression:=AgrParents+ Delta;
Hum.ManID:=manCount;
inc(manCount);
Farea[x,y].Ffamily.APeople[K]:=Hum;
end;

procedure TField.severalManPostcreate(x,y,i:integer);
var olreadyPeople,op,j:integer; Hum:THuman;
begin
olreadyPeople:= Length(FArea[x, y].Ffamily.APeople);
op:= olreadyPeople;
SetLength(FArea[x, y].Ffamily.APeople , op+i);
for j := op to op+i-1 do
begin
Hum := THuman.Create;
Farea[x,y].Ffamily.APeople[j]:=Hum;
end;

end;





function TField.getFamilyId(X, Y: integer): integer;
begin
  result := Farea[x,y].FamilyID;
end;

procedure  TField.SetFamilyId(X, Y: integer);
begin
  inc(Farea[x,y].FamilyID);// !!!! ��������
end;


procedure Tfield.SetCellExist(x,y:integer );
begin

end;

function Tfield.GetCellExist(x,y:integer ):boolean;
begin

end;

function TField.IsInside(X, Y: Integer): boolean;
begin
  Result := (X >= 0) and (X < FWidth) and (Y >= 0) and (Y < FHeight);
end;

constructor TField.Create(Width, Height: Integer);
var
  i, j: Integer;
  cellka: Tcell;
begin
  // ���������� ������ � ������ ����.
  FWidth := Width;
  FHeight := Height;
  isman:=False;
  manCount:=0;
  // ������ ������ ������� FCells.
  SetLength(FArea, FWidth);
  // ������� ������� ������.
  for i := 0 to FWidth - 1 do
  begin
    SetLength(FArea[i], FHeight); // ��� ������������, ������ ���������� ������
    for j := 0 to FHeight - 1 do
    begin
      cellka := Tcell.Create;
      FArea[i, j] := cellka;
    end;
  end;
end;


procedure TField.fillField(rowCount, colCount, maxValue: Integer);
var
  i, j: Integer;
begin
  // ������� ������� ������.
  Randomize;
  for i := 0 to rowCount - 1 do
  begin
    for j := 0 to colCount - 1 do
    begin
      FArea[i, j].Resource := 100;
      // ���������� ������ ��� �������� 100 ����. ��� ��� ���� � ����� ���� ��� ���������� � ���
    end;
  end;
end;

function TField.GetHeight: Integer;
begin
  Result:=FHeight;
end;

function TField.getWidth: Integer;
begin
  Result:=FWidth;
end;

end.
