﻿unit Umodel;


interface

uses Vcl.Grids,  Forms,Windows, Ufield;

type
  Tmodel = class
  private
    FField: TField;
    //procedure SetField(val: TField);
    
    NodesCount : integer;  //    - .   .
    //ClansCount: integer;
   // growthNodes: array of boolean; //      .
    //growthNodes1: array of boolean;
    growthClan : array of boolean;    //        
    growthClan1: array of boolean;    //           
    clans  : array of integer;    //      ()
   // procedure PrepareGrowthNodes();

    //procedure War(x1,y1,x2,y2:integer);//  
        //    (). 2

    procedure GrowGood_2(X, Y: Integer);
    procedure GrowGood_War(X1, Y1,x2,y2: Integer);
    procedure ColonizeCentr();  //            .
    procedure InicialazeModel(Width, Height: Integer);  //        
    function  getNewFamlyId(): integer;   //   id 
    procedure DeathCellsFromRes(x,y:integer); //  ,       (   ?)
    function qwoman(x,y:integer):integer; 
    procedure MigrUntilPeople(x1,y1,x2,y2,forwar:integer);
    procedure ForMigrUntilPeople(x1,y1,x2,y2,forwar:integer;sex:boolean; var k:integer);
    procedure KillUntilPeople(x,y:integer);
    procedure ForKillUntilPeople(x,y:integer;sex:boolean; var k:integer);
    procedure Marriage(x,y,i:integer); //  .
    procedure MarriageForWar(x,y,i:integer); //
    function MarriageAgressiveHusband(x,y:integer):integer;  //в случае обычного брака муж выбирается случайно
    //      в случае голода начинают выбираться наиболее агрессивные мужья ( а жёны?)
    procedure MigrWarAndNoWar (x1,y1,x2,y2,i, forwar:integer;sex:boolean; twoMigr:integer)  ;
    
  public
    maxRes: Integer; //   (   ,     )
    constructor Create(Width, Height: Integer);  //   
 //потребление ресурсов из клетки людьми
    procedure ConsumeLivingResources(X, Y: Integer; Cons: Integer); //      ,        , ,   +    ,  
    procedure GrowAllCells;    //    
    procedure DoStep(step, Width, Height: Integer);   //  
    function findMaxResource(startX, startY: Integer): Integer;// находит макс ресурс вокруг клетки с координатами икс инрек  //     ()
    procedure reproduction(X, Y: Integer); //     
    procedure makeCellExist(X, Y,forwar: Integer; Resource: Integer);  //
    function GetStep: Integer; // на удаление     (  ?)
    procedure AddApeople(x,y,i,husband:integer);// добавляет людей в клетку, в том числе через перемешивание свойств- генов родитплей
    function AAA(x,y:integer):integer;    //AveragApeopleAgression
    var
      Fstep: Integer; // лаг модели

      property Field: TField read FField;
      procedure Reset;
    private
    procedure migration(x1,y1,x2,y2,forwar:integer) ;// миграция ищ один в два  -      1     2
    procedure deadthAge(x,y,i:integer);// сколько гадо убить людкй в клетке на двнном шаге
    procedure killFamily(x,y:integer);// в клетке совсем нет лбдей убрать фамилию
    function QDead(x,y:integer):integer;     // каткое количество мертвых людей на данном шаге в клетке должно быть уничтодено      
    end;

var// экземпляр клксса тмодель? пока что без приставки F
  Model:Tmodel;
  res:integer;
  QdeadPeople:integer;
  AQdeadPeople:array of integer;

var HalfSize:integer; //  ,  координаты середины поля, откуда будут расти все клетки, единая прородина  с блекждеком и евой    


implementation

{procedure Tmodel.War(x1,y1,x2,y2:integer) ;
var i,QPEOPLE:integer;
begin
  qPeople := length(FField.FArea[X2, Y2].Ffamily.APeople);


end;  }

function Tmodel.AAA(x,y:integer):integer;    //AveragApeopleAgression
var qPeople,k, SummAgr:integer ;
begin
   SummAgr:=0;
   qPeople:=length(FField.FArea[x, y].Ffamily.APeople);
   for k:=0 to qPeople-1 do
   begin
      SummAgr:=SummAgr+  FField.FArea[x, y].Ffamily.APeople[k].Agression;
   end;
   Result:= Round(SummAgr/qPeople) ;
end;


procedure Tmodel.KillUntilPeople (x,y:integer); // количество людей после.KillUntilPeople(x,y:integer);
 var qPeopleBegin,k:integer;// сколько лбдей было в клетке до срабатывания процебуры убийства
qpeopleafter:integer;// количество людей в клетке после процедуры убийства
 sex:boolean;//переменна позволяет убить последовательно м или ж одной процедурой
 begin
    qPeopleBegin:=length(FField.FArea[x, y].Ffamily.APeople);
    if qPeopleBegin<1 then exit; // еали людей в клетке нет, то и убиватьтнекого. выход из процедуры 
    sex:=False; //    убиваем женщин    (   )!
    k:=0;
    model.ForKillUntilPeople(x,y,sex, k);// переход на дочернюю пррграмму убийства
    sex:=true; //       убиваем мужчин 
    model.ForKillUntilPeople(x,y,sex, k);     
    qPeopleAfter:=length(FField.FArea[x,y].Ffamily.APeople);
    if (qpeopleBegin <qpeopleafter) then
    Application.MessageBox('  " " ',
      '   ', MB_ICONERROR or MB_OK);
    Exit; // доделать исключение или сообщение
      
 end;

procedure Tmodel.ForKillUntilPeople(x,y:integer;sex:boolean; var k:integer);
var endC,people, i:integer;
begin
  i:=k;
  people:=0;
  endC:= (round(length(FField.FArea[x, y].Ffamily.APeople)/8));
  repeat
      if not (sex=FField.FArea[x, y].Ffamily.APeople[i].Sex)  then
      begin
      Ffield.KillHuman(x,y,i);
      inc(people);
      end ;
    inc(i);  
    if i=0 then  i:= endC;       
    until people >  endC-1; 
    
end;


 procedure Tmodel.MigrUntilPeople(x1,y1,x2,y2,forwar:integer);
 var qPeopleBegin,k:integer;
 sex:boolean;
 begin
    qPeopleBegin:=length(FField.FArea[x1, y1].Ffamily.APeople);
    if qPeopleBegin<1 then exit;
    sex:=False; //
    k:=0;
    FField.FArea[x2, y2].FamilyID:=FamilyCount;
    inc (FamilyCount);
    model.ForMigrUntilPeople(x1,y1,x2,y2,forwar,sex, k);
    sex:=true; //       
    model.ForMigrUntilPeople(x1,y1,x2,y2,forwar,sex, k);
    
 end;

procedure Tmodel.ForMigrUntilPeople(x1,y1,x2,y2, forwar:integer;sex:boolean; var k:integer);
var endC,people,qpeople, alian,i, twoMigr:integer;
begin
  i:=k;
  people:=0;
  endC:= (round(length(FField.FArea[x1, y1].Ffamily.APeople)/8));
  repeat
    alian:= FField.FArea[x1, y1].Ffamily.Alians;
    i:=length (FField.FArea[x1, y1].Ffamily.APeople)-1 ;
    qpeople:=length(FField.FArea[x2, y2].Ffamily.Apeople);
    twoMigr:=1;
      if not (sex=FField.FArea[x1, y1].Ffamily.APeople[i].Sex)  then
        if qpeople>0 then
        begin
        MigrWarAndNoWar (x1,y1,x2,y2,i, forwar, sex,twoMigr)  ;
        end else
        begin
        Ffield.severalManPostcreate(x2,y2,1); //   
        MigrWarAndNoWar  (x1,y1,x2,y2,i, forwar, sex, twoMigr) ;
        end;
    if  (FField.FArea[x1, y1].Ffamily.Alians= alian) then
    i:=i-1;
    inc (people);
    if i=0 then  people:= endC;
    until people >  endC-1;

end;

procedure Tmodel.MigrWarAndNoWar (x1,y1,x2,y2,i, forwar:integer;sex:boolean; twoMigr:integer)  ;

begin
  if (forwar>0) and (twoMigr>1) then
    begin
       if (FField.FArea[x1, y1].Ffamily.APeople[i].FamilyID=FField.FArea[x1, y1].FamilyID)
       then
       begin
        Ffield.cutHuman(x1,y1,x2,y2,i);
       end else inc(FField.FArea[x1, y1].Ffamily.Alians);
    end
  else
            begin
              Ffield.cutHuman(x1,y1,x2,y2,i);
            end;

end ;


procedure Tmodel.AddApeople(x,y,i,husband :integer);
{ i -    Apeople[x,y]
 husband-  }
var
k:integer;
begin
  k:=length(FField.FArea[x, y].Ffamily.APeople) ;
  FField.OneManPostcreate(x,y,k, i,husband);
end;

 //    
procedure Tmodel.DeathCellsFromRes(x,y:integer);
var  manBegin, womanBegin, manSurvived,womanSurvived:integer;
qPeopleBegin,qPeopleSurvived,i,k :integer;
begin
//  ,   ,     
   qPeopleBegin := length(FField.FArea[x,y].Ffamily.APeople); //        
   womanBegin:= qwoman(x,y);
   manBegin:=  qPeopleBegin-  womanBegin;
   manSurvived:= manBegin-Round(qPeopleBegin/4) ;  //        ,      
   womanSurvived:= womanBegin-Round(qPeopleBegin/8) ; //     
   //     ,  
   if FField.FArea[x,y].resource<2 then
   begin
    qPeopleSurvived:=0;//  ,   
   end;

     if qPeopleBegin<5 then
       begin
         manSurvived:= manBegin-1;
         womanSurvived:= womanBegin-1;
       end;
     

   if manSurvived<0 then manSurvived:=0;
   if womanSurvived<0 then womanSurvived:=0;
   if manBegin<0 then manBegin:=0;
   if womanBegin<0 then womanBegin:=0;
   
   
   qPeopleSurvived:=manSurvived+womanSurvived;
   if qPeopleSurvived=0 then killFamily(x,y)
   else
   //  .     20,   15, ,   5
   begin     
      for k := 0 to (qPeopleBegin-qPeopleSurvived) do
        begin
          if FField.FArea[x, y].Ffamily.APeople[k].Sex  then
          Ffield.killHuman(x,y,k);
          WomanSurvived:=WomanSurvived-1;
          qPeopleSurvived:=qPeopleSurvived-1;       
        end  ;

        for k := 0 to qPeopleSurvived do
       begin
          if not FField.FArea[x, y].Ffamily.APeople[k].Sex  then
          Ffield.killHuman(x,y,k);
          WomanSurvived:=manSurvived-1;
          qPeopleSurvived:=qPeopleSurvived-1; 
                
        end  ;
    
     if qPeopleSurvived<0 then  qPeopleSurvived:=0;
    
       
   end;

   
   
   

end;

procedure TModel.killFamily(x,y:integer);
begin

  FField.FArea[x,y].Ffamily:=nil;
end;

procedure TModel.InicialazeModel(Width, Height: Integer);
var i,j:integer;
begin
    i := Width;
    j := Height;
    Tfield.create(i,j);
    Ffield.fillField(i, j, 100);
    ColonizeCentr();
    res := FField.FArea[HalfSize, HalfSize].Resource;   
    growgood_2(HalfSize,HalfSize);; //   
end;

{procedure migration(x1,y1,x2,y2:integer) ;
begin

end;}



function TModel.getNewFamlyId(): integer;   //  
begin
  inc(FamilyCount);
  result := FamilyCount;
end;


constructor TModel.Create(Width, Height: Integer);
begin
  FField := TField.Create(Width, Height);
end;


procedure Tmodel.ConsumeLivingResources(X, Y: Integer; Cons: Integer);
var
  Resources, qPeople: Integer;
begin
    qPeople := length(FField.FArea[X, Y].Ffamily.APeople);
    Resources := FField.FArea[X, Y].Resource - Cons * qPeople;
    if Resources < 0 then
      Resources := 0;
      if qPeople=0 then qPeople:=1;

    if (Round(Resources/qPeople) < 1) and (findMaxResource(x,y)<Round(qPeople/4)) then
    DeathCellsFromRes(x,y);
    //     .
    FField.FArea[X, Y].Resource := Resources;
end;


procedure Tmodel.deadthAge(x,y,i:integer);
begin
  FField.FArea[X, Y].Ffamily.Apeople[i].Death:=true;
end;

procedure Tmodel.GrowAllCells;
var
  qpeople,z,X, Y: Integer;
begin
z:=0;
qpeople:=length(FField.FArea[x, y].Ffamily.Apeople);

  for X := 0 to FField.getWidth - 1 do
  begin
    for Y := 0 to FField.getHeight - 1 do
    begin
      if qpeople>0 then     // ,     .
              GrowGood_2(x,y);
    end;
    end;

  // maxRes == findMaxResource(FWidth, FHeight)
end;



procedure Tmodel.GrowGood_2(X, Y: Integer);
var z,i,qman, qwoman,qpeople, forwar:integer;
begin
z:=0;
QDeadPeople:=-1;
inc(FField.FArea[X, Y].age);
qpeople:= Length(Ffield.FArea[x, y].Ffamily.Apeople);


       begin
          for I := 0 to qpeople-1 do
          begin
            inc(Ffield.FArea[x, y].Ffamily.Apeople[i].age) ;
               deadthAge(x,y,i);
               inc(QDeadPeople);
               Setlength(AQDeadPeople,QDeadPeople+1);
               AQDeadPeople[QDeadPeople]:=i;
            end;
       end;


       if (QDeadPeople>-1)then
       begin
         for I := 0 to QDeadPeople do
            begin
              Ffield.killHuman (x,y,i)
            end;
       end;


      begin
        reproduction(X, Y);
        ConsumeLivingResources(X, Y, 1);
        if (FField.FArea[X, Y].Resource <round( 1.2*qpeople))  then    forwar:=0;
        if (FField.FArea[X, Y].Resource < round(0.8*qpeople))  then    forwar:=1;
      end;
        if( qpeople>0) and (FField.FArea[X, Y].age > 2) and (FField.FArea[X, Y].Resource < 1.2*qpeople)  //////?????
        then
        begin
          maxRes := findMaxResource(X, Y);
          makeCellExist(X, Y,forwar, maxRes);
          end ;
     if (FField.FArea[X, Y].Resource*4< Length(Ffield.FArea[x, y].Ffamily.Apeople)) then
     killFamily(x,y)    ;


end;

       //        
procedure Tmodel.ColonizeCentr();

begin
  HalfSize:=round((Ffield.getWidth-1)/2);
  FField.SetCellExist(HalfSize,HalfSize);
  FField.SeveralManPostcreate(HalfSize,HalfSize,2);
  FField.FArea[HalfSize, HalfSize].alreadyID:=true;
  FField.FArea[HalfSize, HalfSize].FamilyId:=FamilyCount;
  inc(FamilyCount);  //      
end;

procedure Tmodel.DoStep(step, Width, Height: Integer);
var
  i, j: Integer;
  //res: Integer;
begin
  if (step = 0) {and not (FField.FArea[halfSize,halfSize].exist)} then InicialazeModel(Width, Height)
  else Model.GrowAllCells();
end;

function Tmodel.findMaxResource(startX, startY: Integer): Integer;
var
  i, j, res: Integer;
begin
  i := 0;
  j := 0;
  res := FField.FArea[startX, startY].Resource;
  for i := startX - 1 to startX + 1 do
    for j := startY - 1 to startY + 1 do
    begin
      if FField.IsInside(i, j) then
      begin
        if (res < (FField.FArea[i, j].Resource)) and ((i <> startX) and (j <> startY))
        then
          res := FField.FArea[i, j].Resource;
      end;
    end;
  Result := res;
end;

function Tmodel.qwoman(x,y:integer):integer; 
var qwoman,i: Integer;
begin
  qwoman:=0;
    i:=length(FField.FArea[X, Y].Ffamily.APeople);
    for I := 1 to i do
    begin
      if FField.FArea[X, Y].Ffamily.APeople[i-1].Sex=False then
    inc(qwoman);  
    end;
  Result:=qwoman;  
end;

function Tmodel.QDead(x,y:integer):integer; 
var qwoman,i: Integer;
begin
  qwoman:=0;
    i:=length(FField.FArea[X, Y].Ffamily.APeople);
    for I := 1 to i do
    begin
      if FField.FArea[X, Y].Ffamily.APeople[i-1].Sex=False then
    inc(qwoman);  
    end;
  Result:=qwoman;  
end;

procedure TModel.marriage(x,y,i:integer);
{ i -    Apeople[x,y]
 husband-  }
var z,husband:integer;
begin
for  z:=1 to 2 do begin
  husband:=length(FField.FArea[X, Y].Ffamily.APeople)-qWoman(x,y);
  husband:=Random(husband);
  AddApeople(X, Y, i,husband )
end;
end;

procedure Tmodel.MarriageForWar(x,y,i:integer); //
var z, //два раза за шаг модели будет произведено размножение
Allhusband,  //это общее количество мужчин в клетке
 husbandID:integer;//это агрессивные мужчина,выбранный этой женщине в мужья для размножения
begin
for  z:=1 to 2 do begin
  Allhusband:=length(FField.FArea[X, Y].Ffamily.APeople)-qWoman(x,y);
  HusbandID:=MarriageAgressiveHusband(x,y);
  AddApeople(X, Y, i,husbandID )
end;
end;

function Tmodel.MarriageAgressiveHusband(x,y:integer):integer ;
var
forHusband,Allhusband, AgressiveHusbandID :integer;
begin
   Allhusband:=length(FField.FArea[X, Y].Ffamily.APeople)-qWoman(x,y);
   for forHusband := 1 to Allhusband do
   begin
      AgressiveHusbandID:=Random(Allhusband);
     // if True then
      
   end;
   Result:=AgressiveHusbandID;
     

end;

procedure Tmodel.reproduction(X, Y: Integer);
var
  qwoman,i,j: Integer;
begin
    qwoman:=0;
    i:=length(FField.FArea[X, Y].Ffamily.APeople);
    for j := 1 to i do
    begin
      if FField.FArea[X, Y].Ffamily.APeople[j-1].Sex=False then
      inc(qwoman);
    end;
    if (FField.FArea[X, Y].Resource>i) then  //   .  10         
    begin
      for i := 1 to qwoman do marriage(x,y,i);
    end
    else
    begin
     exit
    end;
end;

procedure Tmodel.makeCellExist(X, Y,forwar: Integer; Resource: Integer);
var
  k, i, j,w, res, qpeople: Integer;
begin
  i := 0;
  j := 0;
  qpeople:=length(FField.FArea[i, j].Ffamily.Apeople);
  for i := X - 1 to X + 1 do
    for j := Y - 1 to Y + 1 do
    begin
      if FField.IsInside(i, j) and (qpeople>0) then
      begin
        if ((FField.FArea[i, j].Resource) >= Resource) and not((i = X) and (j = Y))
       //              XY
       //              Resource
        //         
        then
          for w := 0 to forwar do begin
          migration (x,y, i,j,forwar);  //x y-  ,i j -  
          //if forwar>0 then war(x,y,i,j);

          end;
                
      end;
    end;
   maxRes := res;
end;

function Tmodel.GetStep: Integer;
begin
  Result := Fstep;
end;

procedure Tmodel.migration(x1, y1, x2, y2, forwar:integer) ;  //x1 y1-  ,x2, y2:  
//4   4       .      4?
begin
  MigrUntilPeople(x1,y1,x2,y2,forwar); //       1  2
  //GrowGood_War(X1, Y1,x2,y2);
end;



procedure TModel.Reset;
begin     
  FStep := 0;
  FamilyCount := 0;
  NodesCount := 0;
end;

procedure Tmodel.GrowGood_War(X1, Y1,x2,y2: Integer);
var z,i,qman, qwoman,qpeople, forwar:integer;
{  x1 y1     x2 y2. ,    FamilyID
    ,     
    }
begin
z:=0;
QDeadPeople:=-1;
inc(FField.FArea[x1, y1].age);
qpeople:= Length(Ffield.FArea[x1, y1].Ffamily.Apeople);

        for I := 0 to qpeople-1 do
          if (Ffield.FArea[x1, y1].Ffamily.Apeople[i].FamilyID=Ffield.FArea[x1, y1].FamilyID)   then
         forwar:=0;
        if( (qpeople>0)) and (FField.FArea[x1, y1].age > 2) and (FField.FArea[x1, y1].Resource < 1.2*qpeople)  //////?????
        then
        begin
          maxRes := findMaxResource(x1, y1);
          makeCellExist(x1, y1,forwar, maxRes);
          end

end;

end.
