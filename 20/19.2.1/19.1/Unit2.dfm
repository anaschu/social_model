object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 420
  ClientWidth = 733
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    733
    420)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 408
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object StringGrid1: TStringGrid
    Left = 8
    Top = 8
    Width = 721
    Height = 393
    FixedCols = 0
    FixedRows = 0
    TabOrder = 0
    OnDrawCell = StringGrid1DrawCell
    OnSelectCell = StringGrid1SelectCell
    ColWidths = (
      64
      64
      64
      64
      64)
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object GroupBox1: TGroupBox
    Left = 583
    Top = 8
    Width = 185
    Height = 177
    Anchors = [akTop, akRight]
    Caption = 'GroupBox1'
    TabOrder = 1
    object Label3: TLabel
      Left = 96
      Top = 115
      Width = 62
      Height = 13
      Caption = #1088#1072#1079#1084#1077#1088' '#1087#1086#1083#1103
    end
    object StepCountLb: TLabel
      Left = 88
      Top = 144
      Width = 64
      Height = 13
      Caption = #1063#1080#1089#1083#1086' '#1096#1072#1075#1086#1074
    end
    object StartButton: TButton
      Left = 24
      Top = 18
      Width = 75
      Height = 25
      Caption = #1057#1090#1072#1088#1090
      TabOrder = 0
      OnClick = StartButtonClick
    end
    object FieldSizeEdit: TEdit
      Left = 24
      Top = 112
      Width = 57
      Height = 21
      NumbersOnly = True
      TabOrder = 1
      Text = '1'
    end
    object CreateFieldButton: TButton
      Left = 24
      Top = 57
      Width = 75
      Height = 25
      Caption = #1089#1086#1079#1076'. '#1087#1086#1083#1077
      TabOrder = 2
      OnClick = CreateFieldButtonClick
    end
    object StepCountEdit: TEdit
      Left = 24
      Top = 144
      Width = 57
      Height = 21
      TabOrder = 3
      Text = '15'
    end
  end
  object GroupBox2: TGroupBox
    Left = 584
    Top = 224
    Width = 185
    Height = 177
    Anchors = [akTop]
    Caption = 'GroupBox2'
    TabOrder = 2
    object ChBhunger: TDBCheckBox
      Left = 16
      Top = 88
      Width = 97
      Height = 17
      Caption = #1043#1086#1083#1086#1076
      TabOrder = 0
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 600
    Top = 256
  end
end
