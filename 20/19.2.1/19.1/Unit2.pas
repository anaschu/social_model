﻿unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls,
  Forms, Umodel,
  Dialogs, StdCtrls, Vcl.Grids, Vcl.DBCtrls;

type
  TForm2 = class(TForm)
    StringGrid1: TStringGrid;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    StartButton: TButton;//кнопка начала моделирования после заполнения поля ресурсами
    FieldSizeEdit: TEdit;
    Label3: TLabel;
    CreateFieldButton: TButton;// кнопка запрлнения поля ресурсами
    StepCountEdit: TEdit;
    StepCountLb: TLabel;
    SaveDialog1: TSaveDialog;
    ChBhunger: TDBCheckBox;
    procedure StartButtonClick(Sender: TObject);//; ACol, ARow: Integer;   Rect: TRect; State: TGridDrawState);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure CreateFieldButtonClick(Sender: TObject);


  private
    { Private declarations }

    procedure fillStiringGrid(var field: TModel; var sg: TStringGrid;
      { rowCount, colCount, } maxValue: Integer);  //       
  //    

    procedure FormCreate (Sender: TObject);
  public
    { Public declarations }

    
  end;

var
  Form2: TForm2;
  ModExist2:boolean;   //     



implementation

{$R *.dfm}
//   

procedure TForm2.FormCreate(Sender: TObject);
begin
  //   .

  Model := TModel.Create(5,5);
  //  дальше идет переменная, коиорая убирает ошибку. костыль. ч о бы после создания формы прога не думала, что формы нет
  ModExist2:=true;
  StartButton.Enabled := False;

end;

//         
procedure TForm2.CreateFieldButtonClick(Sender: TObject);
var
  Size, E: Integer;
begin
  //  ,
  //  " "   .
  Val(FieldSizeEdit.Text, Size, E);// считываетсч с формы размер поля клеточного автомата
  //   .
  if (E <> 0) or (Size < 1) then begin
    Application.MessageBox('  " " ',
      '   ', MB_ICONERROR or MB_OK);
    Exit;
  end;



  //       (   ).
  CreateFieldButton.Caption := ' ';

  //   . .
  Randomize;


  //    ().
  StringGrid1.RowCount := Size;
  StringGrid1.ColCount := Size;// квадратное поле
  StringGrid1.Visible := True;

  //    .
  Model.Create(Size, Size);
  //   .
  Model.Reset;// сбрасываем все поля в модли на умолчантя
  StartButton.Enabled := True;

  //   ().
 StringGrid1.Refresh;



end;

procedure TForm2.StartButtonClick
  (Sender: TObject { ACol, ARow: Integer;   Rect: TRect; State: TGridDrawState } );
var
//степ каунт- счетчик шагов модели, просто коунт- считанное с формы конечное число шагов
  i, j, Count,FStepCount, Count1, Stepus: integer;//i j -ширина и длинна поля на тоо случай, псли оно нужнг будет прямоугольное
  save:tsavedialog;
  // step:Integer;
//  field: TField;
begin
//Model.Reset;
  ModExist2:=true ;// гворим программе, что форма существует
  {StringGrid1.RowCount := Size;
  StringGrid1.ColCount := Size;         }
  i := StringGrid1.RowCount;
  j := StringGrid1.ColCount; //вводим ширину и длинну поля в модель
  Count := StrToIntDef(StepCountEdit.Text, 0);
  Count1:=0;
  if ModExist2=true then

  Count1:=Model.Fstep;

  if ModExist2=False then Model := TModel.Create(i, j);
  ModExist2:=true;//????? вроде работает трогать не буду
  for FStepCount := 0+Count1 to Count+Count1 do  //
  begin
    Model.DoStep(FStepCount,i,j);// совершаем шаг программы
    if FStepCount=0 then  Model.Fstep:=0;
    StringGrid1.refresh;
    fillStiringGrid(Model, StringGrid1, 10);
    inc(Model.Fstep);
    if not ChBhunger.Checked then Model.Field.fillField(i,j,100);
// увеличили счетчик шагов
  end;
 // запооняем поле стринг грида значнниями из тмодель
  //save:=tsavedialog.Create;// диалогтдля сохранения результатов

end;

procedure TForm2.StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  text: string;// строка для вывкдения в ячейку
  qpeople:integer;
begin
  qpeople:=length(Model.Field.FArea[ACol, ARow].Ffamily.Apeople);

    if not ModExist2  then
    begin
    Model:=Tmodel.Create(5,5);// нужнг пргтив ошибок о не существовании поляe(5,5); //   000004
    ModExist2:=true;
    end;

    StringGrid1.Canvas.Brush.Color:=clRed;
    if ((ACol=HalfSize)and(ARow=HalfSize) and (qpeople>0))then begin
      StringGrid1.Canvas.FillRect(Rect);// сущестаующую центральную клетк
      // ту, в кооорой есть люд
      // //закркшиваем в красный цвет
      StringGrid1.Canvas.Font.Color := clWhite;
      text := StringGrid1.Cells[ACol, ARow];
      DrawText(StringGrid1.Canvas.Handle,
            PChar(text), Length(text), Rect, DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      exit;
    end;

    //Model.Field.FArea[ACol, ARow].exist ;
    if Model.Field.IsInside(ACol, ARow) and (qpeople>0) then begin

    //остальные клетки с свществующии людьми закрашиваем в зелены
    StringGrid1.Canvas.Brush.Color:=clGreen;
      StringGrid1.Canvas.Font.Color := clWhite;
      StringGrid1.Canvas.FillRect(Rect);
  //    text := IntToStr(Field.FArea[ACol, ARow].Resource);
      text := StringGrid1.Cells[ACol, ARow];
      {currentTextWidth := //      Canvas
      StringGrid1.ColWidths[ACol] := max(StringGrid1.ColWidths[ACol], currentTextWidth) }
      DrawText(StringGrid1.Canvas.Handle,
            PChar(text), Length(text), Rect, DT_CENTER or DT_VCENTER or DT_SINGLELINE);
    end
    {else
      begin
         StringGrid1.Canvas.Brush.Color:=clGreen;
        StringGrid1.Canvas.Font.Color := clWhite;
        StringGrid1.Canvas.FillRect(Rect);
      end }
end;


procedure TForm2.StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
var  Resources: String;
Clan:string;
AvAgr:string;
begin
Resources := StringGrid1.Cells[ACol, ARow];
Clan:= InttoStr(Model.Field.FArea[ACol, ARow].FamilyID);
AvAgr:= InttoStr(Model.AAA(ACol, ARow));
Label1.Caption:=
(' Количество еды, тон на 100 км квадратных, количество людей: '+Resources +
', Клан: ' + Clan
+ ', Средняя агрессия ' +AvAgr );
end;

procedure TForm2.fillStiringGrid(var field: TModel; var sg: TStringGrid;
  { rowCount, colCount, } maxValue: Integer);
  // запоонение случайными ресурсами клеток поля. аогда нить пригодитьсчvar
  var
  i, j,qpeople: Integer;
begin
  sg.RowCount := Model.Field.getWidth;
  sg.ColCount := Model.field.getHeight;
  qpeople:=length(Model.Field.FArea[i, j].Ffamily.Apeople);

  // http://programmersforum.ru/showthread.php?t=327138
  //   .
  Randomize;
  for i := 0 to StringGrid1.RowCount - 1 do
  begin
    for j := 0 to StringGrid1.ColCount - 1 do
    begin
      if (qpeople>0) then
      begin
      sg.Cells[i, j] := (inttostr(Model.Field.FArea[i, j].Resource) + ' ' +
        inttostr(length(Model.Field.FArea[i, j].Ffamily.Apeople)));
      end
      else
      sg.Cells[i, j] := (inttostr(Model.Field.FArea[i, j].Resource) );
    end;
  end;
end;

end.
