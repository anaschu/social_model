program LifeMan;

uses
  Forms,
  FMain in 'Sources\FMain.pas' {MainForm},
  ULimits in 'Sources\ULimits.pas',
  UCoord in 'Sources\UCoord.pas',
  UField in 'Sources\UField.pas',
  UModel in 'Sources\UModel.pas',
  UCell in 'Sources\UCell.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.