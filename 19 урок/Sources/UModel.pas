unit UModel;
interface

uses
  SysUtils, ULimits, UCoord, UField, Dialogs , StdCtrls, ULog, Fmain, UCell;


type

// Модель.
  TModel = class
  //проба
  private
    FField: TField; 
	Fstep: integer;

end;	
	
	
	
procedure ConsumeLivingResources(X, Y: Integer; Cons: Integer);
procedure DoStep(Fstep:integer);
procedure GrowAllCells;
function findMaxResource(startX, startY: integer): integer;
procedure reproduction(X,Y:integer);


implementation


procedure Tfield.reproduction (x,y:integer);
var i:integer;
begin
i:= FArea[X, Y].Ffamily.quantity_women;
FArea[X, Y].Ffamily.quantity_women:=i*2;
FArea[X, Y].Ffamily.quantity_man:=i*2;
end;


function TField.findMaxResource(startX, startY: integer ): integer;
var
  i, j, res: integer;
begin
    i := 0;
    j := 0;
    res := FArea[startX, startY].Resource;
    for i := startX - 1 to startX + 1 do
      for j := startY - 1 to startY + 1 do
      begin
        if IsInside(i, j) then begin
          if (res < (FArea[i, j].Resource)) and ((i <> startX) and (j <> startY))
          then
            res := FArea[i, j].Resource;
        end;
      end;
    Result := res;
end;

procedure TField.GrowAllCells;
var
  X, Y: integer;
begin
  for X := 0 to FWidth - 1 do
  begin
    for Y := 0 to FHeight - 1 do
    begin
      if FArea[X, Y].exist then
      begin // Проверяем, что в клетке есть люди.
        inc (FArea[X, Y].age)  ;
        reproduction(x,y);
        ConsumeLivingResources(X, Y, 1);
        if (FArea[X, Y].Age>2) and (FArea[X, Y].resource<10)then
        begin
          MaxRes := findMaxResource(X, Y );
          makeCellExist(X,Y, maxRes);
        end;
      end;
    end;
  end;

  //maxRes == findMaxResource(FWidth, FHeight)
end;
procedure TModel.GrowAllCells;
var
  X, Y: integer;
begin
  for X := 0 to FField.Width - 1 do begin
    for Y := 0 to FField.Height - 1 do begin
      if FField[X, Y].Exists and (not FField[X, Y].alreadyGrow) then begin      // Проверяем, что в клетке есть гриб.
        GrowGood_2(X, Y);
      end;
    end;
  end;
end;

procedure TField.DoStep(Fstep: integer);
var
  i, j: integer;
  res: integer;
begin

  if Fstep = 0 then
  begin
    i := 0;
    j := 0;
    FArea[4, 4].exist:=true;
    FArea[4, 4].Ffamily.quantity_women:=1;
    FArea[4, 4].Ffamily.quantity_man:=1;
    res := FArea[4, 4].Resource;
    for i := 3 to 5 do
      for j := 3 to 5 do
      begin
        if (res <= (FArea[i, j].Resource)) and ((i <> 4) and (j <> 4)) then
          res := FArea[i, j].Resource;
      end;
    maxRes := res;
  end
  else
  begin
    GrowAllCells();
  end;
end;

procedure TField.ConsumeLivingResources(X, Y: Integer; Cons: integer);
var
  Resources, qPeople: integer;

begin

  // Проверяем, что в указанной клетке есть гриб.

  if FArea[X, Y].exist then begin
    // Вычитаем ресурсы.
    qPeople :=  (FArea[X, Y].Ffamily.quantity_man+FArea[X, Y].Ffamily.quantity_women);
    Resources := FArea[X, Y].Resource- Cons*qPeople;
    if Resources < 0 then Resources := 0;
    // Записываем изменения обратно в клетку.
    FArea[X, Y].Resource := Resources;
    // Возвращаем True, если ресурсов больше минимума.

  end;
end;

end.


