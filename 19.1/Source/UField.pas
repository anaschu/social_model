unit UField;

interface

uses Ucell;

type
  TField = class // ��� ����� ���������� ��������� � ����������1
  private
    FWidth: Integer; // ������ ���� � �������
    FHeight: Integer; // ������ ���� � �������
    FField: TField;
    Fstep: Integer;
    maxRes: Integer; // �������� �������� ������ ���� ������
  public
    // �����������.
    FArea: array of array of Tcell; // ������ ������ ����
    constructor Create(Width, Height: Integer);
    // ����������.
    procedure ConsumeLivingResources(X, Y: Integer; Cons: Integer);
    procedure fillField(rowCount, colCount, maxValue: Integer);
    procedure DoStep(Fstep: Integer);
    procedure GrowAllCells;
    function findMaxResource(startX, startY: Integer): Integer;
    function IsInside(X, Y: Integer): boolean;
    procedure reproduction(X, Y: Integer);
    procedure makeCellExist(X, Y: Integer; Resource: Integer);
    function GetHeight: Integer;
    function getWidth: Integer;
    function GetStep: Integer;
    procedure SetStep(Step:Integer);
  end;

implementation

function TField.IsInside(X, Y: Integer): boolean;
begin
  Result := (X >= 0) and (X < FWidth) and (Y >= 0) and (Y < FHeight);
end;

procedure TField.GrowAllCells;
var
  X, Y: Integer;
begin
  for X := 0 to FWidth - 1 do
  begin
    for Y := 0 to FHeight - 1 do
    begin
      if FArea[X, Y].exist then
      begin // ���������, ��� � ������ ���� ����.
        inc(FArea[X, Y].age);
        reproduction(X, Y);
        ConsumeLivingResources(X, Y, 1);
        if (FArea[X, Y].age > 2) and (FArea[X, Y].Resource < 10) then
        begin
          maxRes := findMaxResource(X, Y);
          makeCellExist(X, Y, maxRes);
        end;
      end;
    end;
  end;
  // maxRes == findMaxResource(FWidth, FHeight)
end;

function TField.findMaxResource(startX, startY: Integer): Integer;
var
  i, j, res: Integer;
begin
  i := 0;
  j := 0;
  res := FArea[startX, startY].Resource;
  for i := startX - 1 to startX + 1 do
    for j := startY - 1 to startY + 1 do
    begin
      if IsInside(i, j) then
      begin
        if (res < (FArea[i, j].Resource)) and ((i <> startX) and (j <> startY))
        then
          res := FArea[i, j].Resource;
      end;
    end;
  Result := res;
end;

constructor TField.Create(Width, Height: Integer);
var
  i, j: Integer;
  cell: Tcell;
begin
  // ���������� ������ � ������ ����.
  FWidth := Width;
  FHeight := Height;
  // ������ ������ ������� FCells.
  SetLength(FArea, FWidth);
  // ������� ������� ������.
  for i := 0 to FWidth - 1 do
  begin
    SetLength(FArea[i], FHeight); // ��� ������������, ������ ���������� ������
    for j := 0 to FHeight - 1 do
    begin
      cell := Tcell.Create;
      FArea[i, j] := cell;
    end;
  end;
end;

procedure TField.reproduction(X, Y: Integer);
var
  i: Integer;
begin
  i := FArea[X, Y].Ffamily.quantity_women;
  FArea[X, Y].Ffamily.quantity_women := i * 2;
  FArea[X, Y].Ffamily.quantity_man := i * 2;
end;

procedure TField.makeCellExist(X, Y: Integer; Resource: Integer);
var
  k, i, j, res: Integer;
begin
  i := 0;
  j := 0;
  for i := X - 1 to X + 1 do
    for j := Y - 1 to Y + 1 do
    begin
      if IsInside(i, j) then
      begin
        if ((FArea[i, j].Resource) >= Resource) and not((i = X) and (j = Y))
        then
          FArea[i, j].exist := true;
        FArea[i, j].Ffamily.quantity_man := 4;
        FArea[i, j].Ffamily.quantity_women := 4;
        FArea[4, 4].Ffamily.quantity_women :=
          FArea[4, 4].Ffamily.quantity_women - 4;
        FArea[4, 4].Ffamily.quantity_women :=
          FArea[4, 4].Ffamily.quantity_women - 4;
      end;
    end;
  maxRes := res;
end;

procedure TField.DoStep(Fstep: Integer);
var
  i, j: Integer;
  res: Integer;
begin
  if Fstep = 0 then
  begin
    i := 0;
    j := 0;
    FArea[4, 4].exist := true;
    FArea[4, 4].Ffamily.quantity_women := 1;
    FArea[4, 4].Ffamily.quantity_man := 1;
    res := FArea[4, 4].Resource;
    for i := 3 to 5 do
      for j := 3 to 5 do
      begin
        if (res <= (FArea[i, j].Resource)) and ((i <> 4) and (j <> 4)) then
          res := FArea[i, j].Resource;
      end;
    maxRes := res;
  end
  else
  begin
    GrowAllCells();
  end;
end;

procedure TField.fillField(rowCount, colCount, maxValue: Integer);
var
  i, j: Integer;
begin
  // ������� ������� ������.
  Randomize;
  for i := 0 to rowCount - 1 do
  begin
    for j := 0 to colCount - 1 do
    begin
      FArea[i, j].Resource := 100;
      // ���������� ������ ��� �������� 100 ����. ��� ��� ���� � ����� ���� ��� ���������� � ���
    end;
  end;
end;

procedure TField.ConsumeLivingResources(X, Y: Integer; Cons: Integer);
var
  Resources, qPeople: Integer;
begin

  // ���������, ��� � ��������� ������ ���� ����.
  if FArea[X, Y].exist then
  begin
    // �������� �������.
    qPeople := (FArea[X, Y].Ffamily.quantity_man + FArea[X,
      Y].Ffamily.quantity_women);
    Resources := FArea[X, Y].Resource - Cons * qPeople;
    if Resources < 0 then
      Resources := 0;
    // ���������� ��������� ������� � ������.
    FArea[X, Y].Resource := Resources;
    // ���������� True, ���� �������� ������ ��������.
  end;
end;

function TField.GetHeight: Integer;
begin
  Result:=FHeight;
end;

function TField.getWidth: Integer;
begin
  Result:=FWidth;
end;

function TField.GetStep: Integer;
begin
   Result:=FStep;
 end;

 procedure TField.SetStep(Step:Integer);
 begin
  FStep:=Step;
 end;

 end.
